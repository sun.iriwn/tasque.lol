import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async addComment(commentData) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Comments`)
            .body(commentData)
            .send();
        return response;
    }
}
