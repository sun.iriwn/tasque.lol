import { ApiRequest } from "../request";

const baseUrl: string = global.appConfig.baseUrl;

export class PostsController {
    async getAllPosts() {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("GET")
            .url(`api/Posts`)
            .send();
        return response;
    }

    async createPast(postData) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Posts`)
            .body(postData)
            .send();
        return response;
    }

    async addLike(likeData) {
        const response = await new ApiRequest()
            .prefixUrl("http://tasque.lol/")
            .method("POST")
            .url(`api/Posts`)
            .body(likeData)
            .send();
        return response;
    }
}