import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";

const del = new UsersController();
const auth = new AuthController();

describe("Create new user", () => {
    let accessToken: string;

    before(`Login and get the token`, async () => {
        let response = await auth.login("sun@ukr.net", "123456");

        accessToken = response.body.token.accessToken.token;

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less tthan 5s`).to.be.lessThan(5000);
    });

    it(`Delete user`, async () => {
        let response = await del.deleteUser(2778);
        
        idValue = response.body[1].id;

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less tthan 5s`).to.be.lessThan(5000);
    });
});
