import { expect } from "chai";
import { UsersController } from "../lib/controllers/users.controller";
import { AuthController } from "../lib/controllers/auth.controller";

const users = new UsersController();
const auth = new AuthController();

xdescribe("Update user | with hooks", () => {
    let accessToken: string;
    let userDataBeforeUpdate, userDataToUpdate;

    before(`Login and get the token`, async () => {
        let response = await auth.login("sun@ukr.net", "123456");

        accessToken = response.body.token.accessToken.token;

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    });

    it(`should return correct details of the currect user`, async () => {
        let response = await users.getCurrentUser(accessToken);
        userDataBeforeUpdate = response.body;

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    });

    it(`should update username using valid data`, async () => {
        function replaceLastThreeWithRandom(str: string): string {
            return str.slice(0, -3) + Math.random().toString(36).substring(2, 5);
        }

        userDataToUpdate = {
            id: userDataBeforeUpdate.id,
            avatar: userDataBeforeUpdate.avatar,
            email: userDataBeforeUpdate.email,
            userName: replaceLastThreeWithRandom(userDataBeforeUpdate.userName),
        };

        let response = await users.updateUser(userDataToUpdate, accessToken);
      
        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less tthan 5s`).to.be.lessThan(5000);
    });

    it(`should return correct user details by id after updating`, async () => {
        let response = await users.getUserById(userDataBeforeUpdate.id);
        expect(response.body).to.be.deep.equal(userDataToUpdate, "User details isn't correct");

        expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
        expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    });
});
