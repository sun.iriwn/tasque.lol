import { expect } from "chai";
import { RegisterController } from "../lib/controllers/register.conroller";

const register = new RegisterController();

xdescribe("Create new user", () => {
    it(`Login and get the token`, async () => {
        let response = await register.registretion(0, "string", "sun@ukr.net", "string", "123456");
        
        console.log(response.body)
        expect(response.statusCode, `Status Code should be 201`).to.be.equal(201);
        expect(response.timings.phases.total, `Response time should be less tthan 5s`).to.be.lessThan(5000);
    });
});
